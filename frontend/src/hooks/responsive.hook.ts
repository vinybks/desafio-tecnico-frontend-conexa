import { useMediaQuery } from '@chakra-ui/react';

// mobileS: "320px",
// mobileM: "375px",
// mobileL: "425px",
// tablet: "768px",
// laptop: "1024px",
// laptopL: "1440px",
// desktop: "2560px",
export function useResponsive() {
  const [isTablet] = useMediaQuery('(max-width: 768px)');
  const [isMobile] = useMediaQuery('(max-width: 425px)');

  return {
    isTablet,
    isMobile,
  };
}
