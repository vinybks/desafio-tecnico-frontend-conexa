import cryptoStorage from '../utils/crypto-storage';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useToast } from '@chakra-ui/react';
import { useDispatch } from 'react-redux';
import { authenticateUser } from '../redux/slices/authenticate.slice';

export function useAuthenticateUser() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const toast = useToast();

  useEffect(() => {
    const userLogin = cryptoStorage.getItem('login');

    if (!userLogin) {
      navigate('/');
      toast.closeAll();
      toast({
        title: 'Autenticação',
        description: 'Sessão expirada, por favor, realize o login novamente.',
        status: 'warning',
        duration: 9000,
        isClosable: true,
        position: 'top-right',
      });
      return;
    }
    dispatch(authenticateUser(JSON.parse(userLogin)));
  }, [dispatch, toast]);

  return {};
}
