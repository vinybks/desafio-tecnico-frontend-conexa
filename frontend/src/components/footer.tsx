import { Box, Button, Flex, Text, useDisclosure } from '@chakra-ui/react';
import { useResponsive } from '../hooks/responsive.hook';
import { CreateConsultationModal } from '../pages/dashboard/modals/createConsultation';

export const FooterComponent = () => {
  const {
    isOpen: addConsultationIsOpen,
    onClose: addConsultationOnClose,
    onOpen: addConsultationOnOpen,
  } = useDisclosure();

  const { isTablet, isMobile } = useResponsive();
  const responsive = isTablet || isMobile;

  return (
    <>
      <CreateConsultationModal
        isOpen={addConsultationIsOpen}
        onClose={addConsultationOnClose}
      />
      <Flex
        zIndex={1}
        boxShadow={'rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;'}
        padding={'8px 12px'}
        borderTop={responsive ? '2px solid #DAD2D0' : 'none'}
        backgroundColor={'#FFFFFB'}
        justifyContent={'space-between'}
      >
        <Button
          backgroundColor="transparent"
          border="2px solid"
          color="#2E50D4"
          borderColor="#2E50D4"
          _hover={{ backgroundColor: '#2E50D4', color: 'white' }}
        >
          Ajuda
        </Button>
        <Button
          backgroundColor="#2E50D4"
          border="2px solid"
          color="white"
          borderColor="#2E50D4"
          onClick={addConsultationOnOpen}
          _hover={{ opacity: 0.8 }}
        >
          Agendar Consulta
        </Button>
      </Flex>
    </>
  );
};
