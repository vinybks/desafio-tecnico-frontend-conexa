import { Box, Button, Flex, Img, Text } from '@chakra-ui/react';
import LogoConexta from '../assets/logo-conexa.png';
import { useResponsive } from '../hooks/responsive.hook';
import { useAppSelector } from '../redux/hooks';
import { clearUser, selectUser } from '../redux/slices/authenticate.slice';
import { useNavigate } from 'react-router-dom';
import cryptoStorage from '../utils/crypto-storage';
import { ThemeSwitcher } from './ThemeSwitcher';
import { useDispatch } from 'react-redux';

export const HeaderComponent = () => {
  const { name, token } = useAppSelector(selectUser);
  const { isTablet, isMobile } = useResponsive();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const responsive = isTablet || isMobile;

  return (
    <Flex
      zIndex={1}
      boxShadow={'rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;'}
      padding={'20px 12px'}
      backgroundColor={'#FFFFFB'}
      justifyContent={responsive && !token ? 'center' : 'space-between'}
    >
      <Img src={LogoConexta} width={'12rem'} />
      {/* <ThemeSwitcher /> */}
      {token && (
        <Box display="flex" justifyContent="center" alignItems="center">
          {!responsive && token && (
            <Text
              fontSize={'md'}
              fontWeight="600"
              color="#575453"
              paddingRight="15px"
              fontFamily={'Nunito, sans-serif'}
            >
              {'Olá, Dr. ' + name}
            </Text>
          )}

          <Button
            height="2rem"
            border="2px solid"
            color="#2E50D4"
            borderColor="#2E50D4"
            _hover={{ backgroundColor: '#2E50D4', color: 'white' }}
            onClick={() => {
              cryptoStorage.removeItem('login');
              navigate('/');
              dispatch(clearUser({}));
            }}
          >
            Sair
          </Button>
        </Box>
      )}
    </Flex>
  );
};
