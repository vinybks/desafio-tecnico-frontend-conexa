import {
  RangeDatepicker,
  RangeDatepickerProps,
  SingleDatepicker,
  SingleDatepickerProps,
} from 'chakra-dayzed-datepicker';
import { DATEPICKER_CONFIG } from '../config/datePicker.config';

export function RangePicker({ ...props }: RangeDatepickerProps) {
  return <RangeDatepicker configs={DATEPICKER_CONFIG} {...props} />;
}

export function DatePicker({ ...props }: SingleDatepickerProps) {
  return <SingleDatepicker configs={DATEPICKER_CONFIG} {...props} />;
}
