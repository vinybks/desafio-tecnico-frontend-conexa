import { Flex } from '@chakra-ui/react';
import { ReactNode } from 'react';
import { FooterComponent } from '../components/footer';
import { HeaderComponent } from '../components/header';
import { selectUser } from '../redux/slices/authenticate.slice';
import { useAppSelector } from '../redux/hooks';

type layoutProps = {
  children: ReactNode;
};
export const Layout = ({ children }: layoutProps) => {
  const { token } = useAppSelector(selectUser);

  return (
    <Flex flexDirection={'column'} minHeight="100vh">
      <HeaderComponent />
      <main>{children}</main>
      {token && <FooterComponent />}
    </Flex>
  );
};
