import { API, APITagTypes } from '../../../services/api';

interface CreateConsultationResponse {
  patientIs: number;

  date: Date;
}
interface CreateConsultation {
  patientId: number;
  date: Date;
  token: string;
}

const extendedApi = API.injectEndpoints({
  endpoints: (build) => ({
    createConsultation: build.mutation<
      CreateConsultationResponse,
      CreateConsultation
    >({
      query: ({ patientId, date, token }) => ({
        url: '/consultations',
        method: 'POST',
        body: {
          patientId,
          date,
        },
        headers: {
          authorization: token,
        },
      }),
      invalidatesTags: [APITagTypes.CONSULTATION],
    }),
  }),
});

export const { useCreateConsultationMutation } = extendedApi;
