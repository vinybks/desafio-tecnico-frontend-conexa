import { API } from '../../../services/api';

interface UserAuthenticated {
  token: string;
  name: string;
}
interface ILoginRequest {
  email: string;
  password: string;
}

const extendedApi = API.injectEndpoints({
  endpoints: (build) => ({
    login: build.mutation<UserAuthenticated, ILoginRequest>({
      query: ({ email, password }) => ({
        url: '/login',
        method: 'POST',
        body: {
          email,
          password,
        },
      }),
    }),
  }),
});

export const { useLoginMutation } = extendedApi;
