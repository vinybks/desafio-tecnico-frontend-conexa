import { API, APITagTypes } from '../../../services/api';

export interface Patients {
  id: number;
  patientId: number;
  date: string;
  patient: {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
  };
}

const extendedApi = API.injectEndpoints({
  endpoints: (build) => ({
    getConsultations: build.query<Patients[], { token: string }>({
      query: ({ token }) => ({
        url: '/consultations?_expand=patient',
        method: 'GET',
        queryParams: {
          _expand: 'patient',
        },
        headers: {
          authorization: token,
        },
      }),
      keepUnusedDataFor: 5,
      providesTags: [APITagTypes.CONSULTATION],
    }),
  }),
});

export const { useGetConsultationsQuery } = extendedApi;
