import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export enum APITagTypes {
  CONSULTATION = 'CONSULTATION',
}

export const API = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://localhost:3333',
  }),
  tagTypes: Object.values(APITagTypes),
  endpoints: () => ({}),
});
