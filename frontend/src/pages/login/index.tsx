import { Box, Flex, Img, Text } from '@chakra-ui/react';
import LoginSideImage from '../../assets/boxframe-illustration.png';
import { useResponsive } from '../../hooks/responsive.hook';
import { LoginFormComponent } from './component/loginForm';

export const Login = () => {
  const { isTablet, isMobile } = useResponsive();

  const responsive = isTablet || isMobile;

  return (
    <Flex
      width="100%"
      minHeight={'calc(100vh - 72px)'}
      backgroundColor={'#FFFFFB'}
    >
      {!responsive && (
        <Box
          display="flex"
          width="50%"
          justifyContent="center"
          alignItems={'center'}
          flexDirection={'column'}
        >
          <Img
            src={LoginSideImage}
            maxWidth={'30rem'}
            width="100%"
            height="auto"
          />
        </Box>
      )}

      <Box
        width={isTablet || isMobile ? '100%' : '50%'}
        display="flex"
        justifyContent="center"
        flexDirection="column"
        alignItems="center"
      >
        {responsive && (
          <Text
            fontSize="3rem"
            color="#1C307F"
            fontWeight={700}
            fontFamily="Montserrat, sans-serif"
            paddingBottom="20px"
          >
            Faça Login
          </Text>
        )}

        <LoginFormComponent />
      </Box>
    </Flex>
  );
};
