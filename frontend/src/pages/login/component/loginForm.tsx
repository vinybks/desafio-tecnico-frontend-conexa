import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Stack,
} from '@chakra-ui/react';
import { Field, FieldProps, Form, Formik } from 'formik';
import { useCallback, useEffect } from 'react';
import * as yup from 'yup';
import { useLoginMutation } from '../../../services/api/endpoints/auth.endpoint';
import cryptoStorage from '../../../utils/crypto-storage';
import { useNavigate } from 'react-router-dom';
import { useResponsive } from '../../../hooks/responsive.hook';

const formSchema = yup.object().shape({
  email: yup.string().email().required(),
  senha: yup.string().required(),
});

const initialFormState = {
  email: '',
  senha: '',
};

export const LoginFormComponent = () => {
  const [handleLogin, { isLoading, isSuccess, isError, data }] =
    useLoginMutation();

  const { isTablet, isMobile } = useResponsive();

  const responsive = isTablet || isMobile;

  const navigate = useNavigate();

  const handleSubmit = useCallback(
    (values: { email: string; senha: string }) => {
      handleLogin({
        email: values.email,
        password: values.senha,
      });
    },
    [handleLogin],
  );

  const handleLoading = useCallback(() => {
    return isError ? false : isLoading;
  }, [isError, isLoading]);

  useEffect(() => {
    if (isSuccess) {
      cryptoStorage.setItem('login', JSON.stringify(data));
      navigate('/dashboard');
    }
  }, [data, isSuccess]);

  const loginFormConfig = [
    {
      name: 'email',
      type: 'text',
      placeholder: 'Digite seu e-mail',
      formLabel: 'E-mail',
    },
    {
      name: 'senha',
      type: 'password',
      placeholder: 'Digite sua senha',
      formLabel: 'Senha',
      min: 10,
      max: 20,
    },
  ];
  return (
    <Box
      width={responsive ? '80%' : '45%'}
      color="#575453"
      fontWeight="semibold"
    >
      <Formik
        onSubmit={handleSubmit}
        initialValues={initialFormState}
        validationSchema={formSchema}
      >
        {(props) => (
          <Form>
            <Stack>
              {loginFormConfig.map((loginConfig) => (
                <Field key={loginConfig.name} name={loginConfig.name}>
                  {({ field, form }: FieldProps) => (
                    <FormControl
                      isInvalid={
                        Boolean(form.errors[loginConfig.name]) &&
                        Boolean(form.touched[loginConfig.name])
                      }
                    >
                      <FormLabel htmlFor="email">
                        {loginConfig.formLabel}
                      </FormLabel>
                      <Input
                        {...field}
                        id={loginConfig.name}
                        type={loginConfig.type}
                        placeholder={loginConfig.placeholder}
                        color="#575453"
                        borderColor="#575453"
                        _hover={{ textDecoration: 'none' }}
                      />
                      <FormErrorMessage textTransform={'capitalize'}>
                        {` ${loginConfig.name} é um campo obrigatório`}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                </Field>
              ))}
            </Stack>
            <Flex justifyContent="end" paddingTop="20px">
              <Button
                mt={4}
                type="submit"
                backgroundColor={'#2E50D4'}
                color="white"
                width="100%"
                isLoading={handleLoading()}
                _hover={{ opacity: 0.8 }}
              >
                Entrar
              </Button>
            </Flex>
          </Form>
        )}
      </Formik>
    </Box>
  );
};
