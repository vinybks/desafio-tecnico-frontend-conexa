import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  GridItem,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  SimpleGrid,
} from '@chakra-ui/react';
import { Field, FieldProps, Form, Formik } from 'formik';
import { useCallback, useMemo, useState } from 'react';
import * as yup from 'yup';
import { DatePicker } from '../../../components/datePicker';
import { useAppSelector } from '../../../redux/hooks';
import { selectUser } from '../../../redux/slices/authenticate.slice';
import { useCreateConsultationMutation } from '../../../services/api/endpoints/create-consultation.endpoint';

const addConsultSchema = yup.object().shape({
  patientId: yup.number().required(),
  date: yup.string(),
  hour: yup.string(),
});

const addConsultInitialState = {
  patientId: 0,
  date: new Date('YYYY-MM-DD'),
  hour: '',
};

export type ModalType = {
  isOpen: boolean;
  onClose: () => void;
  onOpen?: () => void;
};

export const CreateConsultationModal = ({ isOpen, onClose }: ModalType) => {
  const { token } = useAppSelector(selectUser);
  const [payload, setPayload] = useState<{ patientId: number; date: Date }>();
  const [selectedDates, setSelectedDates] = useState<Date>(new Date());
  const [selectHour, setSelectHour] = useState<string>('09:00');

  const [handleCreateConsultation, { isLoading, isSuccess, isError }] =
    useCreateConsultationMutation();

  const handleSubmit = useCallback(
    (values: { patientId: number; date: Date }) => {
      const [hour, minutes] = selectHour.split(':');

      const date = new Date(selectedDates).setHours(
        Number(hour),
        Number(minutes),
      );

      setPayload({
        patientId: Number(values.patientId),
        date: new Date(date),
      });

      handleCreateConsultation({
        patientId: values.patientId,
        date: new Date(date),
        token,
      });

      onClose();
    },
    [selectedDates, selectHour],
  );

  const addConsultFormConfig = useMemo(() => {
    return [
      {
        name: 'patientId',
        isSelect: true,
        selectOptions: [1, 2, 3],
        placeholder: 'Insira o ID do paciente',
        formLabel: 'Paciente',
        colSpan: 1,
      },

      {
        name: 'date',
        isDatePicker: true,
        isSelect: false,
        selectOptions: [],
        placeholder: 'Insira a data da consulta',
        formLabel: 'Data',
      },

      {
        name: 'hour',
        isTime: true,
        isSelect: false,
        selectOptions: [],
        placeholder: 'Insira a hora da consulta',
        formLabel: 'Horario',
        colSpan: 1,
      },
    ];
  }, []);

  return (
    <Modal size="xl" isCentered isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <Formik
        initialValues={addConsultInitialState}
        onSubmit={handleSubmit}
        validationSchema={addConsultSchema}
      >
        {(props) => {
          const { resetForm } = props;
          return (
            <Form>
              <ModalContent>
                <ModalHeader fontWeight="bold">Criar nova consulta</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <SimpleGrid columns={2} columnGap={3} rowGap={6} w="full">
                    {addConsultFormConfig.map((addConsultConfig) => (
                      <GridItem
                        colSpan={addConsultConfig.colSpan}
                        key={addConsultConfig.name}
                      >
                        <Field name={addConsultConfig.name} b>
                          {({ field, form }: FieldProps) => (
                            <FormControl
                              isInvalid={
                                Boolean(form.errors[addConsultConfig.name]) &&
                                Boolean(form.touched[addConsultConfig.name])
                              }
                            >
                              <FormLabel htmlFor={addConsultConfig.name}>
                                {addConsultConfig.formLabel}
                              </FormLabel>

                              {addConsultConfig.isSelect && (
                                <Select
                                  {...field}
                                  id={addConsultConfig.name}
                                  placeholder={addConsultConfig.placeholder}
                                >
                                  {addConsultConfig.selectOptions?.map(
                                    (selectOption, key) => (
                                      <option
                                        key={key}
                                        value={`${selectOption}`}
                                      >
                                        {selectOption}
                                      </option>
                                    ),
                                  )}
                                </Select>
                              )}
                              {addConsultConfig.isDatePicker && (
                                <DatePicker
                                  name="date"
                                  date={selectedDates}
                                  id={addConsultConfig.name}
                                  onDateChange={setSelectedDates}
                                />
                              )}

                              {addConsultConfig.isTime && (
                                <Input
                                  id={addConsultConfig.name}
                                  width="50%"
                                  type="time"
                                  name="hour"
                                  onChange={(e) => {
                                    setSelectHour(e.target.value);
                                  }}
                                  min="09:00"
                                  max="18:00"
                                  placeholder={addConsultConfig.placeholder}
                                />
                              )}

                              <FormErrorMessage>
                                {`O campo ${addConsultConfig.formLabel} é obrigatório `}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                      </GridItem>
                    ))}
                  </SimpleGrid>
                </ModalBody>

                <ModalFooter>
                  <Button
                    mr={3}
                    mt={4}
                    onClick={() => {
                      onClose;
                      resetForm();
                    }}
                    backgroundColor="transparent"
                    border="2px solid"
                    color="#FFFFFB"
                    borderColor="#FFFFFB"
                    _hover={{ backgroundColor: 'red', color: 'white' }}
                  >
                    Cancelar
                  </Button>
                  <Button
                    mt={4}
                    type="submit"
                    backgroundColor={'#2E50D4'}
                    color="white"
                    _hover={{ opacity: 0.8 }}
                  >
                    Criar
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
};
