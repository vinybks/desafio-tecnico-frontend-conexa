import { Box, Button, Flex, Text } from '@chakra-ui/react';
import { Patients } from '../../../services/api/endpoints/consultations.endpoint';
import { dateFormat } from '../../../utils/format-date';

export interface ConsultationsType {
  id: number;
  pacientName: string;
  date: string;
}

type IProps = {
  value: Patients;
};

export const ConsultationListComponent = ({ value }: IProps) => {
  const date = dateFormat(value.date);

  return (
    <Flex paddingBottom="20px" justifyContent="space-between">
      <Box flexDirection="column" width="50%">
        <Text
          color="#575453"
          fontSize="16px"
          fontFamily="Nunito, sans-serif"
          noOfLines={1}
        >
          {`${value.patient.first_name} ${value.patient.last_name}`}
        </Text>
        <Text color="#575453" fontSize="12px" fontFamily="Nunito, sans-serif">
          {date}
        </Text>
      </Box>
      <Box paddingRight="5px">
        <Button
          backgroundColor="#2E50D4"
          border="2px solid"
          color="white"
          borderColor="#2E50D4"
          fontFamily="Nunito, sans-serif"
          _hover={{ opacity: 0.8 }}
        >
          Atender
        </Button>
      </Box>
    </Flex>
  );
};
