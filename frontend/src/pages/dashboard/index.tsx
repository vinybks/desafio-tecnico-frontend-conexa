import { Box, Flex, Img, Text, useDisclosure } from '@chakra-ui/react';
import { useAuthenticateUser } from '../../hooks/auth.hook';
import { useResponsive } from '../../hooks/responsive.hook';
import { useAppSelector } from '../../redux/hooks';
import { selectUser } from '../../redux/slices/authenticate.slice';
import { useGetConsultationsQuery } from '../../services/api/endpoints/consultations.endpoint';
import { ConsultationListComponent } from './components/consultationList';
import EmptyList from '../../assets/blank-state.png';

export const Dashboard = () => {
  useAuthenticateUser();
  const { isTablet, isMobile } = useResponsive();
  const responsive = isTablet || isMobile;

  const { token } = useAppSelector(selectUser);

  const { data } = useGetConsultationsQuery({ token }, { skip: !token });

  const responseFromApi = data && data.length > 0 ? data : [];

  return (
    <>
      <Flex
        alignItems="center"
        height={`calc(100vh - ${responsive ? '130px' : '129px'})`}
        flexDirection="column"
        backgroundColor="#FFFFFB"
      >
        <Box width="100%">
          <Text
            color="#1C307F"
            fontSize="calc(44px + 2vmin)"
            fontWeight="bold"
            fontFamily="Nunito, sans-serif"
            paddingLeft="20px"
          >
            Consultas
          </Text>
        </Box>

        {responseFromApi.length > 0 ? (
          <Box
            maxWidth={responsive ? '100%' : '100vh'}
            width={responsive ? '90%' : '50%'}
            minHeight="40vh"
          >
            <Text color="#1C307F" fontWeight="semibold" padding="15px 20px">
              {`${responseFromApi.length} consultas agendadas`}
            </Text>
            <Box
              paddingLeft="20px"
              overflowY="auto"
              maxHeight="calc(100vh - 250px)"
              minHeight="calc(100vh - 250px)"
              __css={{
                '&::-webkit-scrollbar': {
                  w: '1',
                  borderRadius: '5px',
                },
                '&::-webkit-scrollbar-track': {
                  w: '6',
                },
                '&::-webkit-scrollbar-thumb': {
                  borderRadius: '10',
                  bg: `gray.100`,
                },
              }}
            >
              {responseFromApi.map((item) => (
                <ConsultationListComponent key={item.id} value={item} />
              ))}
            </Box>
          </Box>
        ) : (
          <Img src={EmptyList} maxWidth="70%" paddingTop="50px" />
        )}
      </Flex>
    </>
  );
};
