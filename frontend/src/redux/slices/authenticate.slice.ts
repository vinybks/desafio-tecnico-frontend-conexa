import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';

const initialState = {
  name: '',
  token: '',
};

export const authenticateUserSlice = createSlice({
  name: 'user',
  initialState,

  reducers: {
    authenticateUser: (
      state,
      action: PayloadAction<{ name: string; token: string }>,
    ) => {
      const { name, token } = action.payload;

      state.name = name;
      state.token = token;
    },
    clearUser: (state, action: PayloadAction<{}>) => {
      state.token = initialState.token;
      state.name = initialState.name;
    },
  },
});

export default authenticateUserSlice.reducer;

export const selectUser = (state: RootState) => state.user;

export const { authenticateUser, clearUser } = authenticateUserSlice.actions;
