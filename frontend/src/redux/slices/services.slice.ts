import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../redux/store';

const initialState = {
  services: '',
};

export const servicesSlice = createSlice({
  name: 'services',
  initialState,

  reducers: {
    setService: (state, action: PayloadAction<any>) => {
      state.services = 'teste';
    },
  },
});

export default servicesSlice.reducer;
export const serviceState = (state: RootState) => state.serviceState;
export const { setService } = servicesSlice.actions;
