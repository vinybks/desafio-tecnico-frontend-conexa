import {
  Action,
  AnyAction,
  configureStore,
  isRejectedWithValue,
  Middleware,
  ThunkAction,
} from '@reduxjs/toolkit';
import userReducer from './slices/authenticate.slice';
import servicesReducer from './slices/services.slice';
import { API } from '../services/api/index';
import { createStandaloneToast } from '@chakra-ui/react';

interface RejectedAction extends AnyAction {
  error: {
    message: string;
  };
  payload: {
    data: {
      code: string;
      errors: {
        errorCode: string;
        message: string;
      }[];
      message: string;
    };
    status: number | 'FETCH_ERROR';
  };
}

const rtkQueryErrorLogger: Middleware =
  () => (next) => (action: RejectedAction) => {
    const { toast } = createStandaloneToast();

    if (isRejectedWithValue(action)) {
      if (action.payload.status === 'FETCH_ERROR') {
        toast.closeAll();
        toast({
          title: 'Erro',
          description:
            'Não foi possível realizar a operação, por favor, tente mais tarde.',
          status: 'error',
          duration: 9000,
          isClosable: true,
          position: 'top-right',
        });
        return next(action);
      }

      if (!action.payload.data.code) {
        toast.closeAll();
        toast({
          title: 'Erro inesperado',
          description:
            'Não foi possível realizar a operação, por favor, tente mais tarde.',
          status: 'error',
          duration: 9000,
          isClosable: true,
          position: 'top-right',
        });
        return next(action);
      }
      toast.closeAll();
      action.payload.data.errors.map((error) => {
        toast({
          title: error.errorCode || 'Erro',
          description:
            error.message ||
            'Não foi possível realizar a operação, por favor, tente mais tarde.',
          status: 'error',
          duration: 9000,
          isClosable: true,
          position: 'top-right',
        });
      });
    }

    return next(action);
  };

export const store = configureStore({
  reducer: {
    user: userReducer,
    serviceState: servicesReducer,
    [API.reducerPath]: API.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    })
      .concat([API.middleware])
      .concat(rtkQueryErrorLogger),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
