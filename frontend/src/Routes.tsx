import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Layout } from './layouts';
import { Dashboard } from './pages/dashboard';
import { Login } from './pages/login';

export const ProjectRoutes = () => {
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={
            <Layout>
              <Login />
            </Layout>
          }
        />
        <Route
          path="/dashboard"
          element={
            <Layout>
              <Dashboard />
            </Layout>
          }
        />
      </Routes>
    </Router>
  );
};
