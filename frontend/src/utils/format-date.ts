export function dateFormat(userDate: string): string {
  const dateFrom = new Date(userDate).toLocaleDateString('pt-BR');
  const hour =
    new Date(userDate).getHours() < 10
      ? `0${new Date(userDate).getHours()}`
      : new Date(userDate).getHours();
  const minutes =
    new Date(userDate).getMinutes() === 0
      ? '00'
      : new Date(userDate).getMinutes();

  return `${dateFrom} às ${hour}:${minutes}`;
}
