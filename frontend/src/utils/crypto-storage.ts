/* eslint-disable import/no-anonymous-default-export */
import crypto from 'crypto-js';

import { crypto as cryptoConfig } from '../config/index';

function encrypt(value: string, key: string): string {
  const cipher = crypto.AES.encrypt(value, key);
  return cipher.toString();
}

function decrypt(value: string, key: string): string {
  const cipher = crypto.AES.decrypt(value, key);
  return cipher.toString(crypto.enc.Utf8);
}

const setItem = (
  key: string,
  value: string,
  storage = sessionStorage,
): void => {
  const crypt = encrypt(value, cryptoConfig.storageHash);
  storage.setItem(key, crypt);
};

const getItem = (key: string, storage = sessionStorage): string | null => {
  const crypt = storage.getItem(key);
  if (crypt) return decrypt(crypt, cryptoConfig.storageHash);

  return null;
};

const removeItem = (key: string, storage = sessionStorage) => {
  storage.removeItem(key);
};

export default {
  setItem,
  getItem,
  removeItem,
};
