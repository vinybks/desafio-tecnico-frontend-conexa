export const DAY_NAMES = ['Seg', 'Ter', 'Quar', 'Quin', 'Sex', 'Sáb', 'Dom'];
export const MONTH_NAMES = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];
export const DATEPICKER_CONFIG = {
  dateFormat: 'dd/MM/yyyy',
  dayNames: DAY_NAMES,
  monthNames: MONTH_NAMES,
};
export const DATE_VIEW_FORMAT = 'YYYY-MM-DD';
