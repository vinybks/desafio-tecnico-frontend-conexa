# Conexa Saúde Frontend

Projeto desenvolvido para o teste prático da empresa Conexa Saúde

## Tecnologias utilizadas no desenvolvimento

- React
- Vite
- TypeScript
- ChakraUI
- Redux Toolkit
- Crypto-storage

## Oque é e para que serve o projeto desenvolvido

Um frontend que se comunica com uma API assim conseguindo receber e enviar
dados, temos no projeto uma tela inicial que é a tela de login do usuário, e
outra em que o usuário logado possui visão de uma listagem de consultas e
possibilidade de gerenciamento das consultas.

## Arquitetura

Durante o desenvolvimento tentei separar corretamente cada componente para que
não tivesse componentes com código realizando mais do que as suas
responsabilidades principais.

## Linter

Por padrão foi configurado o ESlint com as configurações iniciais que eu estou
acostumado, mas caso exista a necessidade de alterar ou configurar algo a mais
pode ser realizada a alteração.

## Como executar a aplicação

Para instalar as dependências do projeto (Utilizei o Yarn durante o
desenvolvimento).

```bash
yarn install
```

Para executar a aplicação:

```bash
yarn dev
```

Para quem deseja realizar o build da aplicação:

```bash
yarn build
```

Após executar os comandos acima, a aplicação frontend vai ser hosteada em
`http://localhost:3000`

## Como utilizar

Necessariamente a aplicação necessida que o backend disponibilizado dentro do
projeto na estrutura de pastas ./backend esteja rodando.

Para executar o backend deve se direcionar a pasta /backend e utilizar o comando
`yarn start` o mesmo vai ser iniciado por configuração na porta
`http://localhost:3333`.

Com o backend e o frontend rodando, você pode navegar pela aplicação entre a
tela de login e dashboard acompanhando a comunicação entre front e back.

## Responsividade

O front foi desenvolvido para que se adapte aos dispositivos Web e Mobile, sendo
assim os componentes em tela tem comportamento para se adaptar ao dispositivo
utilizado.

## Oque poderia usado / Feito diferente

Acredito que o ponto principal que dificultou um pouco o processo de
configuração do projeto foi a utilização do Vite para iniciar a aplicação,
utilizei para conhecer a ferramenta porém ela dificultou configurações como
_crypto_ e inviabilizou a utilização Jest.

Poderia ter utilizado a criação que estava mais acostumado que é a estrutura do
`create-react-app`.

## Possíveis evoluções

- Melhorar estrutura de temas da aplicação, configurando um arquivo de temas
  para facilitar alterações nos componentes de forma centralizada.
- Adicionar Funcionalidades como atender uma consulta, e uma tela de instruções
  para o usuário.
- Configuração da pagina com PWA para que usuários tenha a possibilidade
  instalar no dispositivo mobile simulando um APP.

## Problemas de Implementação de Testes

Durante o desenvolvimento foi utilizado o Vite para estruturar o projeto, motivo
principal seria conhecer a ferramenta e ver a eficiência pois o Vite configura o
React deixando a comunicação nativa de ESModules que agora os browsers mais
atualizados entendem.

- Porém existem algumas coisas pontuais que o Vite precisa ser configurado
  algumas coisas a mais como para utilizar o Jest. Foram instaladas Dependências
  e configurado arquivos para realizar os testes da aplicação, mas as configs do
  Jest infelizmente não estava entendendo a importação utilizando `import` e o
  transform de TSX.

- Para evitar perder muito tempo nisso realizei as outras especificações do
  projeto, e por fim retornei aos testes mas infelizmente não obtive na
  configuração.

## License

[MIT](https://choosealicense.com/licenses/mit/)
